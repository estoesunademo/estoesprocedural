using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class GenerateMap : MonoBehaviour
{
    [SerializeField] private bool verbose;
    [SerializeField] private GameObject m_Cube;
    public GameObject[,] m_Cubes;

    [Header("Coords")] [SerializeField] private float[,] m_Coords;

    [Header("Size")] [SerializeField] public int width;
    [SerializeField] public int height;

    [Header("Base Parameters")] //
    [SerializeField]
    private float offsetX;

    [SerializeField] private float offsetY;
    [SerializeField] private float frequency = 100f;
    [SerializeField] private float amplitude = 50f;

    //octaves
    private const int MaxOctaves = 8;

    [FormerlySerializedAs("m_Octaves")] [Header("Octave Parameters")] [SerializeField] [Range(0, MaxOctaves)]
    private int octaves;

    [Range(2, 3)] [SerializeField] private int lacunarity = 2;
    [SerializeField] [Range(0.1f, 0.9f)] private float persistence = 0.5f;

    [SerializeField] private bool carve = true;

    private void Start()
    {
        GeneratePerlinMap();
        FillMap();
    }

    private void GeneratePerlinMap()
    {
        m_Coords = new float[width, height];
        m_Cubes = new GameObject[width, height];
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                float perlinNoise = PerlinUtils.CalculatePerlinNoise(x, y, frequency, offsetX, offsetY, octaves,
                    lacunarity, persistence, carve, verbose, true);
                perlinNoise = Mathf.Clamp01(perlinNoise);
                m_Coords[x, y] = perlinNoise;
                GameObject cube = Instantiate(m_Cube);
                //cube.transform.position = new Vector3(x, Mathf.Floor(perlinNoise * amplitude), y);
                cube.transform.position = new Vector3(x, -0.5f, y);
                m_Cubes[x, y] = cube;
            }
        }
    }

    private bool[,] checks;
    public ArrayList Areas { get; set; }

    private void FillMap()
    {
        checks = new bool[width, height];
        Areas = new ArrayList();
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (checks[x, y]) continue;
                float min;
                float max;
                float val = m_Coords[x, y];
                Color col;
                Tipo tipo;
                if (val < 0.4)
                {
                    min = -100f;
                    max = 0.4f;
                    col = Color.red;
                    tipo = Tipo.ZONA_BAJA;
                }
                else if (val < 0.8)
                {
                    min = 0.4f;
                    max = 0.8f;
                    col = Color.green;
                    tipo = Tipo.ZONA_MEDIA;
                }
                else
                {
                    min = 0.8f;
                    max = 100f;
                    col = Color.blue;
                    tipo = Tipo.ZONA_ALTA;
                }

                Area area = new Area(width, 0, height, 0, tipo);
                LookForNeighbours(x, y, min, max, col, area);
                Debug.Log("Area {minX=" + area.MinX + ", maxX=" + area.MaxX + ", minY=" + area.MinY + ", maxY=" +
                          area.MaxY + "}");
                Areas.Add(area);
            }
        }

        Debug.Log("Areas: " + Areas.Count);
        GetComponent<Invent>().PaintMap();
    }

    private void LookForNeighbours(int x, int y, float min, float max, Color col, Area area)
    {
        checks[x, y] = true;
        m_Cubes[x, y].GetComponent<MeshRenderer>().material.color = col;
        area.Cells.Add(new Vector2(x, y));
        if (x < area.MinX)
        {
            area.MinX = x;
        }

        if (x > area.MaxX)
        {
            area.MaxX = x;
        }

        if (y < area.MinY)
        {
            area.MinY = y;
        }

        if (y > area.MaxY)
        {
            area.MaxY = y;
        }

        for (int i = x - 1; i <= x + 1; i++)
        {
            for (int j = y - 1; j <= y + 1; j++)
            {
                if (i < 0 || j < 0 || i >= width || j >= height || checks[i, j]) continue;
                float val = m_Coords[i, j];
                if (val < min || val >= max) continue;
                LookForNeighbours(i, j, min, max, col, area);
            }
        }
    }

    public enum Tipo
    {
        ZONA_BAJA,
        ZONA_MEDIA,
        ZONA_ALTA
    }

    public class Area
    {
        public int MinX;
        public int MaxX;
        public int MinY;
        public int MaxY;
        public readonly List<Vector2> Cells;
        public Tipo tipo;

        public Area(int minX, int maxX, int minY, int maxY, Tipo tipo)
        {
            MinX = minX;
            MaxX = maxX;
            MinY = minY;
            MaxY = maxY;
            Cells = new List<Vector2>();
            this.tipo = tipo;
        }
    }
}