using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using static GenerateMap;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class Invent : MonoBehaviour
{
    [Serializable]
    public struct BiomaInfo
    {
        public string name;
        public Material material;
        public GameObject low;
        public int low_percent;
        public GameObject mid;
        public int mid_percent;
        public GameObject high;
        public int high_percent;

    }
    [SerializeField] private List<BiomaInfo> biomesInfo = new();
    //[SerializeField] private List<Material> biomeMaterials;

    /*[SerializeField] private List<GameObject> desertObjects;
    [SerializeField] private List<GameObject> forestObjects;
    [SerializeField] private List<GameObject> mountainObjects;*/

    private List<List<Area>> biomeAreas;

    private enum Biome
    {
        DESERT,
        FOREST,
        MOUNTAIN
    }

    public void PaintMap()
    {
        ArrayList areas = GetComponent<GenerateMap>().Areas;
        int width = GetComponent<GenerateMap>().width;

        biomeAreas = new List<List<Area>>();
        for (int i = 0; i < 3; i++)
        {
            biomeAreas.Add(new List<Area>());
        }

        foreach (Area area in areas)
        {
            // choose biome depending on where the area is located in the map
            Vector2 araCenter = new Vector2((area.MaxX + area.MinX) / 2, (area.MaxY + area.MinY) / 2);
            int biomeInt = araCenter.x < width / 3 ? (int)Biome.DESERT :
                araCenter.x < 2 * width / 3 ? (int)Biome.FOREST : (int)Biome.MOUNTAIN;
            biomeAreas[biomeInt].Add(area);
            Debug.Log("Biome " + biomeInt);
        }

        for (int i = 0; i < 3; i++)
        {
            FillBiome(biomeAreas[i], i);
        }
    }

    private void FillBiome(List<Area> areas, int biome)
    {
        foreach (Area area in areas)
        {
            FillArea(area, biomesInfo[biome]);
        }
    }

    private void FillArea(Area area, BiomaInfo bioma)
    {
        Debug.Log("Cells count: " + area.Cells.Count);
        int count = 0;
        List<Vector2> cellsToFill = new List<Vector2>();

        // paint the cells with the object
        foreach (Vector2 cell in area.Cells)
        {
            GetComponent<GenerateMap>().m_Cubes[(int)cell.x, (int)cell.y].GetComponent<MeshRenderer>().material = bioma.material;
        }
        int percent;
        GameObject obj;
        switch (area.tipo)
        {
            case Tipo.ZONA_ALTA:
                percent = bioma.high_percent;
                obj = bioma.high;
                break;
            case Tipo.ZONA_MEDIA:
                percent = bioma.mid_percent;
                obj = bioma.mid;
                break;
            default:
                percent = bioma.low_percent;
                obj = bioma.low;
                break;
        }
        while (count < area.Cells.Count / percent)
        {
            Vector2 cell = area.Cells[Random.Range(0, area.Cells.Count)];
            if (cellsToFill.Contains(cell)) continue;
            cellsToFill.Add(cell);
            count++;
            Instantiate(obj, new Vector3(cell.x, 0.5f, cell.y), Quaternion.identity);
        }
    }
}